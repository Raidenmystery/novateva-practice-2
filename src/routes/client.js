'use strict'

const express = require('express');
const ClientCtrl = require('../controllers/client.js');
const api_client = express.Router();

api_client.get('/client/:ClientId', ClientCtrl.getClient);
api_client.get('/client', ClientCtrl.getClients);
api_client.post('/client', ClientCtrl.saveClient);
api_client.put('/client/:ClientId', ClientCtrl.updateClient);
api_client.delete('/client/:ClientId', ClientCtrl.deleteClient);

module.exports = api_client;