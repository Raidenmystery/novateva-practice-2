'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = Schema
({
    Name: String,
    Amount: Number,
    Category:
    {
        type: String,
        enum: ['vegetable','fruit','sausage','meat']
    },
    Price: Number
});

module.exports = mongoose.model('Product', ProductSchema);