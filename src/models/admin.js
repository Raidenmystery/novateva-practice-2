'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdminSchema = Schema
({
    Name: String,
    Id: 
    {
        type: String,
        unique: true
    },
    Age: Number,
    Gender: 
    {
        type: String,
        enum: ['female','male']
    },
    Date: Date,
    Address: String,
    User: 
    {
        type: String,
        unique:true
    },
    Password: String,
    Email:
    {
        type: String,
        unique:true
    }
});

module.exports = mongoose.model('Admin', AdminSchema);