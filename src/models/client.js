'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientSchema = Schema
({
    Name: String,
    Id: 
    {
        type: String,
        unique: true
    },
    Age: Number,
    Gender: 
    {
        type: String,
        enum: ['female','male']
    },
    Date: Date,
    Address: String
});

module.exports = mongoose.model('Client', ClientSchema);