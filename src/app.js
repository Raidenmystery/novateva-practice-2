'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const api_admin = require('./routes/admin.js');
const api_client = require('./routes/client.js');
const api_product = require('./routes/product.js');

//Middleware
app.use(bodyParser.urlencoded(
  {
     extended: false
  }));
app.use(bodyParser.json());

app.use('/api',api_admin);
app.use('/api',api_client);
app.use('/api',api_product);

module.exports = app;