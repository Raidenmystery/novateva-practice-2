'use strict'

const Admin = require('../models/admin.js');

function getAdmin (req, res)
{
    let AdminId = req.params.AdminId;

    Admin.findById(AdminId, (err, Admin) =>
    {
        if (err) return res.status(500).send({message: `Error al realizar la peticion : ${err}`});

        if (!Admin) return res.status(404),send({ message: `El trabajador no existe`});

        res.status(200).send({ Admin: Admin});

    });
}

function getAdmins (req, res)
{
    Admin.find({}, (err, Admins) => 
      {
        if (err) return res.status(500).send(
          {
             message: `Error al realizar la peticion : ${err}`
          });

        if (!Admins) return res.status(404).send(
          {
             message: 'No existe personal administrativo'
          });

        res.status(200).send(
          {
             Admins: Admins
          });

      });
}

function saveAdmin (req, res)
{
    //console.log('\nPost /api/admin\n');
    //console.log(req.body);

    let admin = new Admin();

    admin.Name = req.body.Name;
    admin.Id = req.body.Id;
    admin.Age = req.body.Age;
    admin.Gender = req.body.Gender;
    admin.Date = req.body.Date;
    admin.Address = req.body.Address;
    admin.User = req.body.User;
    admin.Password = req.body.Password;
    admin.Email = req.body.Email;

    if ( admin.Age <= 10) res.status(400).send( {message: `\nError al salvar en la base de datos: Edad no valida ${admin.Age}`});

    admin.save((err, adminRegistered) => 
    {
        if (err) res.status(500).send( {message: `\nError al salvar en la base de datos: ${err}`});

        res.status(200).send({admin: adminRegistered});

    });
    
}

function updateAdmin (req, res) 
{
    let AdminId = req.params.AdminId;
    let update = req.body;

    Admin.findByIdAndUpdate(AdminId, update, (err, AdminUpdated) => 
    {
        if (err) res.status(500).send({message: `Error al actualizar el empleado: ${err}`});

        res.status(200).send({ Admin: AdminUpdated });
    });

}

function deleteAdmin (req, res) 
{
    let AdminId = req.params.AdminId;

    Admin.findById((AdminId), (err, Admin) => 
    {
        if (err) res.status(500).send({message: `Error al borrar el empleado: ${err}`});

        Admin.remove(err => 
        {
            if (err) res.status(500).send({message: `Error al borrar el empleado: ${err}`});
            res.status(200).send({message: `El empleado ha sido eliminado`});            
        });
    })
}

module.exports = 
{
    getAdmin,
    getAdmins,
    saveAdmin,
    updateAdmin,
    deleteAdmin
}